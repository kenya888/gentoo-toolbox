# base image
FROM docker.io/gentoo/stage3:systemd

ENV NAME=gentoo-toolbox
ARG DIST="http://ftp.iij.ad.jp/pub/linux/gentoo/snapshots"
ARG PORTAGE_FILE=portage-latest.tar.xz
ARG PORTAGE_DIR=var/db/repos

LABEL com.github.containers.toolbox="true" \
      com.github.debarshiray.toolbox="true" \
      name="gentoo-toolbox" \
      usage="This image is meant to be used with the toolbox command" \
      summary="Base image for creating Gentoo toolbox containers" \
      maintainer="Takahiro Hashimoto <kenya888@gmail.com>"

COPY README.md /

WORKDIR /

COPY extra-packages /
RUN curl -LO "${DIST}/${PORTAGE_FILE}" \
 && mkdir -p "${PORTAGE_DIR}" \
 && tar xpf "${PORTAGE_FILE}" --xattrs-include='*.*' --numeric-owner -C "${PORTAGE_DIR}" \
 && mv "${PORTAGE_DIR}"/portage "${PORTAGE_DIR}"/gentoo \
 && emerge --sync \
 && MAKEOPTS="-j$(nproc)" emerge --jobs=2 -uvDN @world \
 && MAKEOPTS="-j$(nproc)" emerge --jobs=2 -v $(<extra-packages) \
 && sed -i -e 's/^#.*wheel.*)\sALL.*/%wheel ALL=(ALL) ALL/' /etc/sudoers \
 && rm -rf var/cache/distfiles var/cache/binpkgs var/cache/edb/dep mnt media home /"${PORTAGE_FILE}"

CMD /bin/sh
