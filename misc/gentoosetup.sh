#!/bin/bash

ACCEPT_KEYWORDS_DIR=/etc/portage/package.accept_keywords

#sudo rm /etc/portage/make.conf

echo "GENTOO_MIRRORS=\"http://ftp.iij.ad.jp/pub/linux/gentoo http://ftp.riken.jp/pub/Linux/gentoo\"" | sudo tee -a /etc/portage/make.conf
echo "EMERGE_DEFAULT_OPTS=\"--jobs=2\"" | sudo tee -a /etc/portage/make.conf
echo "MAKEOPTS=\"-j8\"" | sudo tee -a /etc/portage/make.conf
echo "LINGUAS=\"ja\"" | sudo  tee -a /etc/portage/make.conf
sudo mkdir ${ACCEPT_KEYWORDS_DIR}
echo "sys-kernel/linux-headers ~amd64" | sudo tee -a ${ACCEPT_KEYWORDS_DIR}/kernel
echo "sys-kernel/gentoo-sources ~amd64" | sudo tee -a ${ACCEPT_KEYWORDS_DIR}/kernel

sudo emerge --sync
sudo emerge -v eix vim tmux gentoo-sources
sudo emerge -C nano
sudo eix-update

sudo emerge -uvDN @world
