#!/bin/bash

echo "sys-devel/clang default-lld" | sudo tee -a /etc/portage/package.use/clang

sudo emerge -v clang llvm libcxx libcxxabi compiler-rt llvm-libunwind lld

sudo mkdir /etc/portage/env
sudo cp compiler-clang /etc/portage/env/
sudo cp package.env /etc/portage/

